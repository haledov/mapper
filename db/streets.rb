require 'csv'
Street.destroy_all
csv_text = File.read(Rails.root.join('lib', 'seeds', 'ts.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'cp1251')
csv.each do |row|
  puts row.to_hash
   t = Street.new
   t.name = row[0]
   t.save
  puts "#{t.name} saved"
end