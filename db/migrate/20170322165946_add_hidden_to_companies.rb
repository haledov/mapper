class AddHiddenToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :hidden, :boolean
  end
end
