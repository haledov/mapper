class AddSluggedToStreets < ActiveRecord::Migration
  def change
    add_column :streets, :slug, :string
  end
end
