class AddLatitudeAndLongitudeToStreet < ActiveRecord::Migration
  def change
    add_column :streets, :latitude, :float
    add_column :streets, :longitude, :float
  end
end
