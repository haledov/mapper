class AddStreetAndHnumToCompanies < ActiveRecord::Migration
  def change
    add_reference :companies, :street, index: true, foreign_key: true
    add_column :companies, :hnum, :string
  end
end
