class AddCityToCompany < ActiveRecord::Migration
  def change
    add_reference :companies, :city, foreign_key: true

  end
end
