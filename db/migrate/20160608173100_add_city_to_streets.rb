class AddCityToStreets < ActiveRecord::Migration
  def change
    add_reference :streets, :city, index: true, foreign_key: true
  end
end
