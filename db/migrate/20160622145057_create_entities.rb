class CreateEntities < ActiveRecord::Migration
  def change
    create_table :entities do |t|
      t.string :name
      t.text :description
      t.string :phone
      t.string :address
      t.string :url

      t.timestamps null: false
    end
  end
end
