class AddWebsiteAndDescriptioToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :website, :string
    add_column :companies, :description, :text
  end
end
