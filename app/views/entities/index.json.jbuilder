json.array!(@entities) do |entity|
  json.extract! entity, :id, :name, :description, :phone, :address, :url
  json.url entity_url(entity, format: :json)
end
