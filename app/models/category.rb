class Category < ActiveRecord::Base
extend ActsAsTree::TreeWalker
	 acts_as_tree order: "name"
	 has_many :companies

	 def self.top
	  where(parent_id:  'NULL')
	end

	def has_children?
	  children.size > 0
	end

end
