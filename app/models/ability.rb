class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)

    if user.encrypted_password? # Non-admin user
      can :read, :all
      can :my, Company
      can :new, Company
      can :create, Company
    end

    if user.admin? # Admin user
      can :manage, :all
    end
  end
end
