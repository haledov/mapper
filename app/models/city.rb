class City < ActiveRecord::Base
	has_many :companies
	has_many :streets

  def name
    self[:ru].presence || self[:name]
  end

end
