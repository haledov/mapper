class Street < ActiveRecord::Base
	belongs_to :city
	has_many :companies
  geocoded_by :name   # can also be an IP address
  after_validation :geocode
	acts_as_mappable :default_units => :kms,
                     :default_formula => :sphere,
                   	 :distance_field_name => :distance,
                   	 :lat_column_name => :latitude,
                   	 :lng_column_name => :longitude



end
