class Company < ActiveRecord::Base

	belongs_to :city
	belongs_to :category
	belongs_to :street
	belongs_to	:user
	acts_as_mappable :default_units => :kms,
                   	 :lat_column_name => :lat,
                   	 :lng_column_name => :lng
	scope :approved, -> {where(:hidden => false)}

end
