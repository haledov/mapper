class ApplicationController < ActionController::Base

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  before_filter :fetch_cities, :set_locale, :fetch_last_companies, :fetch_categories, :alphabet
  before_filter :ensure_signup_complete, only: [:new, :create, :update, :destroy]

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden }
      format.html { redirect_to signin_path, :alert => exception.message }
    end
  end

protected
  def alphabet
    @alphabet = "A Ą B C Č D E Ę Ė F G H I Į Y J K L M N O P R S Š T U Ų Ū V Z Ž".split(" ")

  end

	def fetch_cities
  		@cities = Rails.cache.fetch('cities_list', :expires_in=>10.minutes) do
  		City.all
  		end
	end

  def fetch_last_companies
      @companies_cache = Rails.cache.fetch('companieslast', :expires_in=>10.minutes) do
      Company.approved.limit(10).order(created_at: :desc)
      end
  end
  def fetch_categories
    @categories = Rails.cache.fetch('categories_list', :expires_in=>10.minutes) do
      Category.all
    end
    end

  def authorize_admin
      redirect_to :back, status: 401 unless current_user.admin
      #redirects to previous page
  end


  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def ensure_signup_complete
  # Убеждаемся, что цикл не бесконечный
  return if action_name == 'finish_signup'

  # Редирект на адрес 'finish_signup' если пользователь
  # не подтвердил свою почту
  if current_user && !current_user.email_verified?
    redirect_to finish_signup_path(current_user)
  end
end
end
