class CompaniesController < ApplicationController
  load_and_authorize_resource :except => [:list, :show]
  before_filter :authorize_admin, only: :destroy
  before_action :set_company, only: [:show, :edit, :update, :destroy]
  before_action :set_loc, only: [:create, :update]

  # GET /companies
  # GET /companies.json
  def index
    #@companies = Company.all
    redirect_to my_path
  end

  def my
    if(current_user.admin?)
      @companies = Company.all.order(created_at: :desc)
    else
    @companies = Company.where(user_id: current_user).order(created_at: :desc)
    end
  @markers ||=[]
  @companies.each do |company|
    puts company.lat
    linker = ActionController::Base.helpers.link_to(company.name, company_path(company.id))
    @markers << {:latlng => [company.lat, company.lng], :popup => linker + '\n' + company.street.name}

    end
  end


  def list
    @category = Category.find(params[:cid])
    @companies = Company.approved.where(category_id: params[:cid]).order(created_at: :desc)
    @markers ||=[]
    @companies.each do |company|
      puts company.lat
      linker = ActionController::Base.helpers.link_to(company.name, company_path(company.id))
      @markers << {:latlng => [company.lat, company.lng], :popup => linker + '\n' + company.street.name}

    end

  end

  # GET /companies/1
  # GET /companies/1.json
  def show
    if @company.hidden? and !current_user.admin?
      redirect_to root_path, notice: 'Такой компании не существует'
    end
    #@location = Geokit::Geocoders::MultiGeocoder.geocode @city.name+@street.name+@company.hnum.to_s
  end

  # GET /companies/new
  def new
    @company = Company.new
  end

  # GET /companies/1/edit
  def edit
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = Company.new(company_params)

    @company.lat = @location.lat
    @company.lng = @location.lng
    @company.hidden = true
    @company.user = current_user
    respond_to do |format|
      if @company.save
        format.html { redirect_to my_path, notice: 'Организация успешно добавлена!' }
        format.json { render :show, status: :created, location: @company }
      else
        format.html { render :new }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /companies/1
  # PATCH/PUT /companies/1.json
  def update

    respond_to do |format|
      if @company.update(company_params)
        format.html { redirect_to my_path, notice: 'Данные организации успешно обновлены.' }
        format.json { render :show, status: :ok, location: @company }
      else
        format.html { render :edit }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company.destroy
    respond_to do |format|
      format.html { redirect_to my_url, notice: 'Company was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    def set_loc
      @city = City.find(params[:company][:city_id])
      @street = Street.find(params[:company][:street_id])
      @address = @city.name+', '+@street.name+', '+params[:company][:hnum].to_s
      puts "****************************************"
      puts @address
      @location = Geokit::Geocoders::MultiGeocoder.geocode @address
      @company.lat = @location.lat
      @company.lng = @location.lng
      @company.address = @address
      puts @company.to_yaml
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.fetch(:company, {}).permit(:name, :hidden, :street_id, :hnum, :city_id, :category_id, :description, :phone, :website, :worktime)
    end
end
