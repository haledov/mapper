class PagesController < ApplicationController
  def index
     @markers ||=[]
    @companies_cache.each do |company|
      puts company.lat
      linker = ActionController::Base.helpers.link_to(company.name, company_path(company.id))
      @markers << {:latlng => [company.lat, company.lng], :popup => linker + '\n' + company.street.name}
    end
  end

  def signin
  end

  def welcome
  end

  def about
  end
end
