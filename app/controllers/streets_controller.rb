class StreetsController < ApplicationController
  load_and_authorize_resource :except => [:index, :list, :show]
  before_action :set_street, only: [:show, :edit, :update, :destroy]


  # GET /streets
  # GET /streets.json
  def index
    @streets = Street.all
  end

  def list
    puts params
    @city = City.find_by("name=? OR ru=?", params[:city], params[:city])
    puts @city
    @streets = Street.where("city_id=? AND name LIKE ?",@city, params[:letter]+'%')
    #@location = Geokit::Geocoders::MultiGeocoder.geocode @city.name

  end
  # GET /streets/1
  # GET /streets/1.json
  def show
    puts @street.companies
    puts 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
  end

  def fix
    streets = Street.all
    streets.each do |str|
      puts str.name.strip
      str.update_attribute(:name, str.name.strip)
    end
    render :nothing => true, :status => 200, :content_type => 'text/html'
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_street
      @street = Street.find(params[:id])

      @city = City.find(@street.city_id)
      #@location = Geokit::Geocoders::MultiGeocoder.geocode @city.name+', '+@street.name

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def street_params
      params.require(:street).permit(:name, :city, :letter)
    end
end
