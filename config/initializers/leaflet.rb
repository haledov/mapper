Leaflet.tile_layer = "http://a.tile.osm.org/{z}/{x}/{y}.png"
# You can also use any other tile layer here if you don't want to use Cloudmade - see http://leafletjs.com/reference.html#tilelayer for more
Leaflet.attribution = "HOM.LT 2017"
Leaflet.max_zoom = 18